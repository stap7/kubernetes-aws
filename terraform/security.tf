resource "aws_security_group" "web-node" {
  name = "web-node"
  description = "Web Security Group"
  vpc_id      = aws_vpc.default.id
  ingress {
      protocol = "-1"
      from_port = 0
      to_port = 0
      cidr_blocks = ["10.0.0.0/24"]
  }
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }    
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}