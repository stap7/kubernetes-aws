provider "aws" {
    access_key = "AKIAXEAA4PE7WXRL7MXI"
    secret_key = "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"
    region = "us-east-1"
}

resource "aws_vpc" "default" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.default.id
}

resource "aws_route_table" "rtb_public" {
  vpc_id = aws_vpc.default.id
  route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.gw.id
    }
}

resource "aws_route_table_association" "rta_subnet_public" {
  subnet_id      = aws_subnet.tf_meetup_subnet.id
  route_table_id = aws_route_table.rtb_public.id
}

resource "aws_subnet" "tf_meetup_subnet" {
  vpc_id                  =  aws_vpc.default.id
  cidr_block              = "10.0.0.0/24"
  map_public_ip_on_launch = true
  depends_on = ["aws_internet_gateway.gw"]
}

resource "aws_instance" "k8-master" {
  ami = "ami-0c322300a1dd5dc79"
  instance_type = "t2.medium"
  private_ip = "10.0.0.11"
  subnet_id  = aws_subnet.tf_meetup_subnet.id
  key_name = "myec2key"
  vpc_security_group_ids = ["${aws_security_group.web-node.id}"]
}

resource "aws_instance" "k8-node" {
  ami = "ami-0c322300a1dd5dc79"
  instance_type = "t2.micro"
  private_ip = "10.0.0.12"
  key_name = "myec2key"
  subnet_id  = aws_subnet.tf_meetup_subnet.id
  vpc_security_group_ids = ["${aws_security_group.web-node.id}"]
}

