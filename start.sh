#!/bin/sh

# change to terraform dir
cd terraform

# apply change on testing infrastructure
terraform apply
sleep 20

# store inventory so that to be used by Ansible
terraform-inventory --inventory ./ > ../ansible/inventories/hosts

# change to ansible dir
cd ../ansible

echo Ready

# ANSIBLE_HOST_KEY_CHECKING=False ansible -i inventories/hosts all -m ping -u ec2-user --private-key=myec2key.pem
# ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i inventories/hosts playbook.yml --private-key=myec2key.pem -u ec2-user
# rm -rf ~/.kube && scp -i myec2key.pem -r ec2-user@ec2-54-162-146-184.compute-1.amazonaws.com:~/.kube ~/